// package name: json2csv
package main

import (
	"C"
	"bytes"
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"path/filepath"
	"plugin"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/oschwald/geoip2-golang"
	"github.com/ua-parser/uap-go/uaparser"
)

const (
	BO_TmFmt   = "2006-01-02 15:04:05"
	BO_TMPFILE = 0x410000

//	GEO_DB_NAME    = "GeoLite2-City.mmdb"
//	UA_PARSER_YAML = "regexes.yaml"
)

var (
	once   sync.Once
	OutDir string
	BOTz   *time.Location

	GeoDB    *geoip2.Reader
	UAParser *uaparser.Parser
)

// These are output for C to avoid GC
var (
	out_err_ptr []string
	out_tbl_ptr [][]string
	out_fd_ptr  [][]int
)

//====================streamer 3.0 test below===============================
var (
	//	config = flag.String("config", "json_config.json", "config json") // ?? 可以設成區域變數?
	//	config              = flag.String("config", "json_config.json", "config json") // ?? 可以設成區域變數?
	config              *string
	allfield_generation = flag.Bool("allfield", false, "wheather to auto generate all fields from input source")

	debug = flag.Bool("debug", false, "turn on debug mode")

	loadconfig LOADCONFIG
	datasource DATASOURCE

	list_redundant_fields bool = false
	boCol                      = map[string]map[string]int{}         // $$ map[Workspace.Table][key] = 在tablesetting裡的順序
	boColOrder                 = map[string]map[string]map[int]int{} // $$ map[ver][Workspace.Table][在tablesetting裡的順序] = _bo的順序
	//	colData    = map[string]map[string]map[string]string{} // $$ map[ver][Workspace.Table][key] = data

	// $$ bo_fields = map[ver][Workspace.Table][_bo的順序] = _bo的欄位資訊
	// $$ 		  --> map[ver][Workspace.Table][ boColOrder[ver][Workspace.Table][在tablesetting裡的順序] ] = _bo的欄位資訊
	// $$ 		  --> map[ver][Workspace.Table][ boColOrder[ver][Workspace.Table][ boCol[Workspace.Table][key] ] ] = _bo的欄位資訊
	bo_fields = map[string]map[string][]BO_FIELD{} //bo columns based on data_structure's Order,

	// $$ 為了在 parsing 的時候, 可以使用別的欄位 parsing 的結果, 所以必須要把整個 bo_fields 傳給.so檔.
	// $$ 而因為裡面有 BO_FIELD, 所以要先把它轉成map
	// $$ bo_fields_map 其實跟 bo_fields 一模一樣, 只是把BO_FIELD取代成map, 為了讓 .so檔可以順利使用
	// $$ map[ver][Workspace.Table][_bo的順序] = _bo的欄位資訊
	bo_fields_map = make(map[string]map[string][]map[string]interface{})

	key_func = map[string]map[string]func(map[string]string) (bool, error){} // map[ver]map[key]func( record[key]value ) (row_success, error)

	customfunc []*plugin.Plugin

	CacheFunc = make(map[string]interface{})
	//	topic     = strings.TrimSuffix(filepath.Base(*config), ".json")
	topic string
)

type LOADCONFIG struct {
	TableSetting  map[string]interface{} `json:"table_settings"`
	Setting       map[string]interface{} `json:"source_setting"`
	DataStructure map[string]interface{} `json:"data_structure"`
}

type DATASOURCE struct {
	source_type        string
	all_version_struct *All_Version_Struct
	//	source_json *Source_Json // $$ in source_json.go
	//	source_csv  *Source_Csv
	//	source_url  *Source_Url
}

type All_Version_Struct struct {
	store_struct map[string]reflect.Type
	version      []string
	//	field_key    map[string][]string
}

type BO_FIELD struct {
	Workspace   string
	Table       string
	Fieldname   string
	Datatype    string
	Order       int
	Datacontent string
	Keys        []string
	Parse       map[string]interface{}
}

//====================streamer 3.0 test above===============================

func WriteToFile(records [][]string) (fd int, err error) {

	if fd, err = syscall.Open(OutDir, syscall.O_RDWR|BO_TMPFILE|syscall.O_EXCL|syscall.O_CLOEXEC, 0666); err != nil {
		return fd, err
	}

	wBuffer := new(bytes.Buffer)
	wBuffer.Grow(8196)
	w := csv.NewWriter(wBuffer)
	w.WriteAll(records)
	if err := w.Error(); err != nil {
		syscall.Close(fd)
		fd = -1
		return fd, err
	}

	syscall.Write(fd, wBuffer.Bytes())
	return fd, nil
}

func TranslateCdn(ver string, version_struct interface{}, customfunc []*plugin.Plugin, tables_rows map[string][][]string, fatal_error *string) map[string][][]string {

	//======================== streamer 3.0 test below ==============================
	// $$ temp_tables_rows只存這筆資料parse的結果, 而傳進來的tables_rows, 則可能包含之前幾筆資料的所有結果 (尤其是CSV)
	// $$ 只有當這筆資料確定完全parse成功時 (row_success == true), 才把temp_tables_rows的內容加在tables_rows上
	temp_tables_rows := make(map[string][][]string)
	var row_success bool = true

	colData := make(map[string]map[string]map[string]string)
	// $$ 先把資料從 version_struct 裡提出來, 按照 _bo裡定義的 Fieldname 塞到colData
	//assign content to column, wstbl is Workspace.Table
	colData[ver] = make(map[string]map[string]string)
	for wstbl, bofield_content := range bo_fields[ver] {
		colData[ver][wstbl] = make(map[string]string)
		for _, fld := range bofield_content {
			Debug(wstbl, " ", fld.Keys, " ", fld.Fieldname)
			//	fmt.Println(wstbl, " ", fld.Keys, " ", fld.Fieldname)
			colData[ver][wstbl][fld.Fieldname] = InterfaceToStr(get_JsonFieldName(reflect.ValueOf(version_struct).Elem(), fld.Keys).Interface())
		}
	}

	//	fmt.Println("colData: ", colData)

	/*
		   $$
		       ex:
		           bo_fields 是先用workspace + Table 分類
		           , bo_fields[ver]["Auto.Storm"] 裡面存的是在 struct.json的 _bo 裡欄位, 按順序塞進去 <---在此我稱為 "_bo的順序"

		           boCol 是用來 mapping bo_fields 和 boColOrder
		           , boCol["Auto.Storm"]["dt"] = table_setting 中的順序

		           boColOrder 可以透過boCol的mapping, 到回去 bo_fields 裡撈欄位的相關資訊
		           boColOrder[ver]["Auto.Storm"][table_setting 中的順序] = _bo的順序

				   boCol[wstbl][field_name] = table_setting 中的順序
				   boColOrder[ver][wstbl][table_setting 中的順序] = _bo的順序

				   也就是說 bo_fields[ver]["Auto.Storm"][ boColOrder[ver]["Auto.Storm"][在tablesetting裡的順序] ] = 欄位的相關資訊

				   *** table_setting 中的順序 可能為負值: 當欄位有定義 _bo卻沒定義 table_setting時

	*/
	//check if need to parse content and prepare one row array for data append

	// $$ 可以把parsing的結果, 或parsing一半以後需要被別的欄位使用的結果 存起來
	Parse_result_cache := make(map[string]map[string]string) // $$ map[wstbl]map[key]parse_result

	for wstbl, bofield_content := range bo_fields[ver] { // $$ wstbl = Workspace + Table
		if !row_success {
			break //
		}
		var row []string
		Parse_result_cache[wstbl] = make(map[string]string)
		/*
			$$ 原本有一個問題, 就是 config 裡定義的 _bo 欄位必須要跟 table_setting 裡定義的一樣, 就算不再用的欄位, 也要寫 _bo 並且塞空白
			$$ 否則沒寫 _bo 的欄位, 因為 boColOrder[ver][wstbl][i] 不存在, 導致 fldidx 為0, 也就是 colData會去抓 bofield_content[0]的值 (通常是 ts)

			$$ 這裡修正 boColOrder[ver][wstbl][i] 必須要存在才繼續parsing, 不存在則塞空白
			$$ 而原本的 for i := 0; i < len(boColOrder[ver][wstbl]) && row_success; i++ { 不能用

			$$ 因為如果有欄位沒有定義 _bo 的話, len(boColOrder[ver][wstbl]) != table_setting的長度,
			$$ 假設 table有 7 個欄位, 其中 2 個欄位沒有定義 _bo,  len(boColOrder[ver][wstbl])會是 5, 也就是說 for迴圈少跑 2 次, 造成最後 2 個欄位會是空白
			$$ 所以改成 len(boCol[wstbl]), 也就是 table的欄位數

		*/

		//	for i := 0; i < len(boColOrder[ver][wstbl]) && row_success; i++ {

		// $$ 以下此for迴圈是為了組一個row
		for i := 0; i < len(boCol[wstbl]) && row_success; i++ {

			data := ""
			var fldidx int
			var exist bool
			if fldidx, exist = boColOrder[ver][wstbl][i]; exist {
				// $$ 代表此欄位有寫 _bo

				data = colData[ver][wstbl][bofield_content[fldidx].Fieldname]
				//	fmt.Println("i: ", i, ", exist:", exist, ", fldidx: ", fldidx)
				// $$ fldidx = "_bo的順序", 也就是 bo_fields[ver]["Auto.Storm"] 裡的順序
				// $$ 而這裡的 _ 其實就是 "在tablesetting裡的順序", 因為我們用range讀出 boColOrder的內容, 所以其實也是照 "在tablesetting裡的順序" 在寫資料
				//	fmt.Println("bofield_content[fldidx].Fieldname: ", bofield_content[fldidx].Fieldname, ", colData: ", data)
				if len(bofield_content[fldidx].Parse) > 0 { // $$ 裡面有定義parse的fuction
					//	fmt.Println("Parse: ", bofield_content[fldidx].Fieldname)
					var parse_result string
					var parse_err error
					//	parse_result, Parse_result_cache, row_success, parse_err = parse_func(customfunc, colData[ver][wstbl], bofield_content[fldidx].Fieldname, data, bofield_content[fldidx].Parse, bofield_content, Parse_result_cache)

					target_table := wstbl
					target_key := bofield_content[fldidx].Fieldname
					//	bo_fields_map := bo_fields[ver]
					bo_fields_map_ver := bo_fields_map[ver]

					parse_result, Parse_result_cache, row_success, parse_err = parse_func(customfunc, colData[ver], target_table, target_key, bo_fields_map_ver, boColOrder[ver], boCol, Parse_result_cache)

					if parse_err != nil {
						//	fmt.Println(parse_err.Error())
					}
					// $$ 如果row_success為false, 則整個row都drop 掉
					if row_success == false {
						row = nil
						if parse_err != nil {
							if *fatal_error != "" {
								*fatal_error = *fatal_error + ";"
							}
							*fatal_error = *fatal_error + parse_err.Error()
						}

						break
					}
					row = append(row, parse_result)
					data = parse_result

					Debug("parse", bofield_content[fldidx].Fieldname, "::", data, ".................", parse_result)
				} else {
					//	fmt.Println("No Parse: ", bofield_content[fldidx].Fieldname)
					row = append(row, data)
					//	Debug("no parse", colData[ver][wstbl][bofield_content[fldidx].Fieldname])
				}
			} else {
				// $$ 代表此欄位在config裡沒有寫 _bo
				//	fmt.Println("i: ", i, ", exist:", exist, ", fldidx: ", fldidx)
				row = append(row, data)
			}
		}

		if row_success && row != nil {
			temp_tables_rows[wstbl] = append(tables_rows[wstbl], row)
		}
	}

	// $$ ======================================
	if row_success { // $$ 確定整筆資料都parse成功
		for wstbl, rows := range temp_tables_rows {
			if len(rows) > 0 { // $$ 這個table有東西要寫, 都把它塞給正是的tables_rows
				tables_rows[wstbl] = append(tables_rows[wstbl], rows...)
			}
		}
	}
	/*
		if row_success {
			for wstbl, rows := range temp_tables_rows {
				for _, row := rows {
					if len(row) > 0 { // $$ 這個table有東西要寫, 都把它塞給正是的tables_rows
						tables_rows[wstbl] = append(tables_rows[wstbl], row)
					}
				}

			}
		}
	*/

	return tables_rows

}

//export SetCounter
func SetCounter(_ string, _ int) {
}

//export SetConfig
func SetConfig(_ string) {
}

//export JsonToCsv
func JsonToCsv(inFd []int, outPath string) (out_tbl [][]string, out_fd [][]int, err_str []string) {
	once.Do(func() {
		//	fmt.Println("test Leo -1 sss")
		var err error

		if BOTz, err = time.LoadLocation("Asia/Taipei"); err != nil {
			log.Fatalln("Load TW time zone fail:", err)
		}
		//	fmt.Println("test leo -1.0.1")
		//	if GeoDB, err = geoip2.Open("../common/" + GEO_DB_NAME); err != nil {
		//		log.Fatalln("Open geo db fail:", err)
		//	}

		//	UAParser, err = uaparser.New("../common/" + UA_PARSER_YAML)

		OutDir = outPath
		//	fmt.Println("test leo -1.0.2")
		customfunc = load_plugin()
		//	fmt.Println("test leo -1.0.3")
		//	config_setting(*config)
		config_setting()
		//	fmt.Println("test leo -1.0.4")
		topic = strings.TrimSuffix(filepath.Base(*config), ".json")
		//	fmt.Println("test leo -1.0.5")
		//initial version parsing structure
		//	fmt.Println("test leo -1.1")
		datasource.source_type = loadconfig.Setting["source_type"].(string)

		if loadconfig.Setting["list_redundant_fields"] != nil {
			list_redundant_fields = loadconfig.Setting["list_redundant_fields"].(bool)
		}

		//	fmt.Println("test leo -1.2")
		// $$ 依照json config產生structure
		//	if datasource.source_type == "json" { //dynamic generate json struct based on json data_structure
		//		datasource.source_json = GetJsonStruct()
		//	} else if datasource.source_type == "csv" {
		//		datasource.source_csv = GetCsvStruct()
		//	} else if datasource.source_type == "url" {
		//		datasource.source_url = GetUrlStruct()
		//	}
		datasource.all_version_struct = datasource.GetAllVersionStruct()
		// ?? Leo: 有需要每筆資料都挖? 還是在scan 外面挖一次就好??

		var bo_struct_err error
		// $$ turn map[string]map[string][]BO_FIELD{} --> map[string]map[string][]map[string]interface{}
		if bo_fields_map, bo_struct_err = turn_bo_struct_to_map(bo_fields, bo_fields_map); bo_struct_err != nil {
			/*	row_success = false
				if *fatal_error != "" {
					*fatal_error = *fatal_error + ";"
				}

				*fatal_error = *fatal_error + bo_struct_err.Error()
			*/
			//	fmt.Println("bo_fields_map error : ", bo_struct_err)
			log.Fatal("bo_fields_map error : ", bo_struct_err)
		}

		prepare_table()
		//	fmt.Println("bo_fields: ", bo_fields)

		//	fmt.Println("boColOrder: ", boColOrder)

		//	fmt.Println("boCol:", boCol)

		//	fmt.Println("bo_fields_map: ")
		//	fmt.Println(bo_fields_map)

	})

	var wg sync.WaitGroup
	// $$ 我猜 infd是一個csv檔有幾個row
	err_str = make([]string, len(inFd))
	out_tbl = make([][]string, len(inFd))
	out_fd = make([][]int, len(inFd))

	for idx, jFd := range inFd {
		wg.Add(1)
		go func(idx, jFd int) {
			defer func() {
				wg.Done()
			}()

			//========================================
			var inStat syscall.Stat_t
			if err := syscall.Fstat(jFd, &inStat); err != nil {
				fmt.Printf("Get input file state fail: %v\n", err)
				err_str[idx] = err.Error()
				return
			}

			source_data, err := syscall.Mmap(jFd, 0, int(inStat.Size), syscall.PROT_READ, syscall.MAP_SHARED)
			if err != nil {
				fmt.Printf("Mmap input file fail: %v\n", err)
				err_str[idx] = err.Error()
				return
			}
			//	fmt.Println("source_data: ", string(source_data))
			//	fmt.Println(string(source_data[1]))
			defer syscall.Munmap(source_data)
			//========================================
			var fatal_error string

			tables_rows := make(map[string][][]string)

			var checkversion interface{}
			//	fmt.Println("datasource.source_type: ", datasource.source_type)

			//	err_str[idx] = "It is error"
			//	return

			if datasource.source_type == "json" || datasource.source_type == "gamania_json" {
				// $$ 這裡假設一個 json 檔裡只有一筆資料
				// $$ 把json config裡的_version結構挖出來, 造一個checkversion的structure, 再用Unmarshal去data source裡面挖version實際的值
				// $$ 目的是想知道 source data中的version藏在哪
				checkversion = reflect.New(datasource.all_version_struct.store_struct["_version"]).Interface()
				//	fmt.Println("checkversion: ", checkversion)
				source_data = bytes.Replace(source_data, []byte("NaN"), []byte("0"), -1)
				source_data2 := source_data
				// $$ =============for gamania========================
				if datasource.source_type == "gamania_json" {
					if source_data, err = put_outer_data_in_json(source_data); err != nil {
						// $$ 提取json 前面的 [time],[ip]
						err_str[idx] = "put_outer_data_in_json: " + err.Error() // + ": " + string(source_data2) + "==>" + string(source_data)
						return
					}
					if source_data, err = replace_invalid_characters(source_data); err != nil {
						// $$ 提取json 前面的 [time],[ip]
						err_str[idx] = "replace_invalid_characters " + err.Error() // + ": " + string(source_data2) + "==>" + string(source_data)
						return
					}
				}

				//====================streamer 3.0 test below===============================
				// $$ Leo: 用剛剛拿到的checkversion結構, 去source_data中挖出真正要哪一個version
				if err = json.Unmarshal([]byte(source_data), &checkversion); err != nil {
					err_str[idx] = "use checkversion get version fail: " + err.Error() + ": " + string(source_data2) // + "==>" + string(source_data)
					return
				}
				//=======================================================
				// $$ Leo: 現在checkversion已經有值了 ex: { Query: { Stm: 1557997753559}}
				// $$       接著再用 datasource.all_version_struct.version ([Query Stm]) 和 get_JsonFieldName 去提取 Stm 中的 1557997753559
				// $$											data reflect.Value, 					fields []string
				// $$

				ver := InterfaceToStr(get_JsonFieldName(reflect.ValueOf(checkversion).Elem(), datasource.all_version_struct.version).Interface())
				//		fmt.Println("version:",ver)
				//if no version is found, use default data structure
				if datasource.all_version_struct.store_struct[ver] == nil {
					ver = "_default"
				}
				//use the actual version struct to decode json
				// $$ 真正version的值已經有了, 就可以去 datasource.source_json.store_struct 把她的struct挖出來 用Unmarshal parsing
				version_struct := reflect.New(datasource.all_version_struct.store_struct[InterfaceToStr(ver)]).Interface()
				//	fmt.Println("version_struct")
				//	fmt.Println(version_struct)
				if err = json.Unmarshal([]byte(source_data), &version_struct); err != nil {
					ver_msg := "cannot find any struct of version " + InterfaceToStr(ver) + ": " + string(source_data)
					//	fmt.Println(ver_msg)
					err_str[idx] = err.Error() + ": " + ver_msg
					return
				}
				// $$ 真正開始parse資料
				// parse data and prepare for BO Table
				if datasource.all_version_struct.store_struct[ver] != nil {
					tables_rows = TranslateCdn(ver, version_struct, customfunc, tables_rows, &fatal_error)
				}

				// $$ ==========List redundant fields========================
				if list_redundant_fields == true {
					redundant_fields := make(map[string]interface{})
					input_keys := make(map[string]interface{})
					if err = json.Unmarshal([]byte(source_data), &input_keys); err != nil {
						ver_msg := "List redundant fields: cannot put source data into maps"
						fmt.Println(ver_msg)
						err_str[idx] = err.Error() + ver_msg
						return
					} else {
						redundant_fields = List_redundant_fields(input_keys, reflect.ValueOf(version_struct).Elem())
						fmt.Println("redundant_fields: ", redundant_fields)
					}
				}

				//==================================

			} else if datasource.source_type == "csv" {
				ver := "_default"
				// $$ 這裡假設一個csv檔裡有很多筆資料
				if datasource.all_version_struct.store_struct[ver] == nil {
					ver = "_default"
				}
				version_struct := reflect.New(datasource.all_version_struct.store_struct[InterfaceToStr(ver)]).Interface()

				//==========================================
				var reader = csv.NewReader(bytes.NewReader(source_data))
				//	reader.Comma = '\t'                  // $$ 設定分隔字元

				//	reader.Comma = loadconfig.Setting["Comma"].(rune) //','
				reader.Comma = ','
				//	fmt.Println("reader.Comma: ", reader.Comma)
				reader.Comment = '#'                 // $$ 設定註解字元
				csv_records, err := reader.ReadAll() // $$ 一次讀完整個csv
				if err != nil {
					log.Println("Read csv fail:", err)
					err_str[idx] = err.Error()
					return
				}
				// ?? pre_source_cnt的功能還沒加
				for _, row := range csv_records {
					if row_success, err := CsvUnmarshal(row, reflect.ValueOf(version_struct).Elem()); row_success {
						tables_rows = TranslateCdn(ver, version_struct, customfunc, tables_rows, &fatal_error)
					} else {
						err_str[idx] = err.Error()
					}

				}

				// ====================================
			} else if datasource.source_type == "url" {

				// $$ sss?A=1&B=2&C=3
				// $$ 要先做 url.Parse(string(source_data)) 這一步
				// $$ , 如果直接用 url.ParseQuery(source_data), 則問號後方的值(也就是A)拿不到
				//	u, _ := url.Parse(string(source_data))
				//	url_map, _ := url.ParseQuery(u.RawQuery) //

				// $$ 拿?以後的值
				// $$ 為何不用上方先Parse的作法? 因為如果網址裡面有 : 符號 (ex: [2019-07-06 23:03:59]/beanfun/3XKD55nAM9/i?appleIdfv=17E7DBB8-760B-4779-9919-F85F656BCA7B), Parse就不能用了
				source_data = source_data[strings.Index(string(source_data), "?")+1:]
				url_map, _ := url.ParseQuery(string(source_data))
				//===========================
				checkversion = reflect.New(datasource.all_version_struct.store_struct["_version"]).Interface()

				// checkversion的結構有兩種可能: 第一層: Version, 或超過一層 Query => Dtm => ....
				var ver string
				version_inside_struct := false

				first_checkversion := reflect.TypeOf(checkversion).Elem().Field(0)

				// $$ 這個if 希望可以拿到struct, 如果不加Kind(), 就會變成 struct { OOXX OOXX }
				if first_checkversion.Type.Kind().String() == "struct" {
					version_inside_struct = true
				}
				if version_temp_key, ok := first_checkversion.Tag.Lookup("json"); ok {
					if version_inside_struct { // $$ 代表_version不是在第一層的url, 而是在json內
						// $$ 假設 checkversion 的結構只能吃 {"query":{"dtm":OOXX}}
						// $$ 但 url_map.Get(version_temp_key) 的到的結果卻是 {"dtm":OOXX}
						// $$ 所以要把它拼回去 checkversion 的json格式
						ver_json_string := "{\"" + version_temp_key + "\"" + ":" + url_map.Get(version_temp_key) + "}"
						if err = json.Unmarshal([]byte(ver_json_string), &checkversion); err != nil {
							fmt.Println(err.Error())
							err_str[idx] = err.Error() + ": " + string(ver_json_string)
							return
						} else {
							ver = InterfaceToStr(get_JsonFieldName(reflect.ValueOf(checkversion).Elem(), datasource.all_version_struct.version).Interface())
						}
					} else { // // $$ 代表_version是在第一層的url, ex: ...&version=321
						ver = url_map.Get(version_temp_key) // $$ 取得 321
					}
				}

				if datasource.all_version_struct.store_struct[ver] == nil {
					ver = "_default"
				}
				fmt.Println("ver: ", ver)

				version_struct := reflect.New(datasource.all_version_struct.store_struct[InterfaceToStr(ver)]).Interface()

				// $$ 目前先跟讀csv寫一模一樣
				/*
					var reader = csv.NewReader(bytes.NewReader(source_data))
					//	reader.Comma = '\t'                  // $$ 設定分隔字元

					//	reader.Comma = loadconfig.Setting["Comma"].(rune) //','
					reader.Comma = ','
					fmt.Println("reader.Comma: ", reader.Comma)
					reader.Comment = '#'                 // $$ 設定註解字元
					url_records, err := reader.ReadAll() // $$ 一次讀完整個csv
					if err != nil {
						log.Println("Read url fail:", err)
						err_str[idx] = err.Error()
						return
					}
				*/

				if row_success, err := UrlUnmarshal(url_map, reflect.ValueOf(version_struct).Elem()); row_success {
					tables_rows = TranslateCdn(ver, version_struct, customfunc, tables_rows, &fatal_error)
				} else {
					err_str[idx] = err.Error()
				}

				// $$ ==========List redundant fields========================
				if list_redundant_fields == true {
					redundant_fields := make(map[string]interface{})
					input_keys := make(map[string]interface{})
					for key, value := range url_map {
						input_keys[key] = value
					}
					//	fmt.Println("input_keys: ", input_keys)
					//	fmt.Println("version_struct: ", reflect.ValueOf(version_struct).Elem().Type())

					redundant_fields = List_redundant_fields(input_keys, reflect.ValueOf(version_struct).Elem())
					fmt.Println("redundant_fields: ", redundant_fields)
				}

				// $$ ==================================

			}

			//	fmt.Println("tables_rows: ", tables_rows)
			table_fd := make(map[string]int)
			for wstbl, rows := range tables_rows {
				if len(rows) > 0 { // $$ 這個table有東西要寫, 就把該table所有的rows一次寫進去, 並return fd
					if fd, err := WriteToFile(rows); err != nil {
						log.Println("write ", wstbl, " data fail:", err)
					} else {
						regexp_wstbl := regexp.MustCompile("(.+)\\.(.+)").FindStringSubmatch(wstbl)
						if len(regexp_wstbl) >= 3 { // $$ 0 is Workspace.Table, 1 is Workspace, 2 is Table
							table_fd[regexp_wstbl[2]] = fd
						}

					}
				}
			}

			out_tbl[idx] = []string{}
			out_fd[idx] = []int{}

			err_str[idx] = fatal_error

			for wstbl, fd := range table_fd {
				//	fmt.Println("wstbl: ", wstbl, ", fd: ", fd)
				out_tbl[idx] = append(out_tbl[idx], []string{wstbl}...)
				out_fd[idx] = append(out_fd[idx], []int{fd}...)

			}
			//	for key, _ := range table_fd {
			//		delete(table_fd, key)
			//	}

		}(idx, jFd)
	}
	wg.Wait()

	// keep last output reference to avoid GC after return to C program
	out_err_ptr = err_str
	out_tbl_ptr = out_tbl
	out_fd_ptr = out_fd

	return out_tbl, out_fd, err_str
}

func main() {
}

//====================streamer 3.0 test below===============================

func Debug(text ...interface{}) {
	if *debug {
		fmt.Println(text...)
	}
}

func load_plugin() (customfunc []*plugin.Plugin) {
	//	fmt.Println("test load_plugin 1")
	files, err := ioutil.ReadDir("./")
	if err != nil {
		//		fmt.Println("test load_plugin 2", err)
		log.Fatal(err)
	}
	//	fmt.Println("test load_plugin 3")

	for _, f := range files {
		//		fmt.Println("file name is ", f.Name())
		findfilename := regexp.MustCompile("(.+)\\.so").FindStringSubmatch(f.Name())

		if findfilename != nil {
			//	fmt.Println(findfilename[0])
			funcfile, err := plugin.Open(findfilename[0])
			if err != nil {
				fmt.Println("Open ", findfilename[0], "fail: ", err)
			} else {
				fmt.Println(f.Name(), " is loaded")
				customfunc = append(customfunc, funcfile)
			}
		}
	}
	return customfunc

}

func config_setting( /*config string*/ ) {

	//load config
	//	fmt.Println(config)
	files, err := ioutil.ReadDir("./")
	if err != nil {
		log.Fatal("load ReadDir fail: ", err)
	}
	for _, f := range files {

		findfilename := regexp.MustCompile("(.+)\\_config.json").FindStringSubmatch(f.Name())

		if findfilename != nil {
			config = &findfilename[0]
			load_json, err := ioutil.ReadFile(findfilename[0])

			if err != nil {
				log.Fatal("ReadFile ", findfilename[0], " failed: ", err)
			}
			if err = json.Unmarshal([]byte(load_json), &loadconfig); err != nil {
				log.Fatal("json.Unmarshal ", string(load_json), " failed: ", err)
			}

			fmt.Println(findfilename[0], " is loaded")
			break
		}

	}
}
func InterfaceToStr(value interface{}) string /*, error */ {

	switch value.(type) {
	case string:
		return value.(string) //, nil
	case []string:
		return value.([]string)[0] //, nil
	case int:
		return strconv.Itoa(value.(int)) //, nil
	case int32:
		return strconv.Itoa(int(value.(int32))) //, nil
	case int64:
		return strconv.FormatInt(int64(value.(int64)), 10) //, nil
	case float64:
		return strconv.FormatFloat(value.(float64), 'f', -1, 64) //, nil
	case nil:
		return ""
	default:
		temp := fmt.Sprintf("InterFace type %T convert to string error,\n", value)
		log.Println(temp)
		return "" //, fmt.Errorf(temp)
	}
}

func (datasource *DATASOURCE) GetAllVersionStruct() *All_Version_Struct {

	datasource.all_version_struct = new(All_Version_Struct)
	var s *All_Version_Struct = datasource.all_version_struct
	s.store_struct = make(map[string]reflect.Type)
	//	s.field_key = make(map[string][]string)

	/*
		$$
		$$	loadconfig.DataStructure: 各version的DataStructure
		$$	"1557997753559", "1.03" 等等都是version的名稱

			1557997753559: ....
			1.03: ......

		$$	除了 "_version", 是用來記錄一筆資料的version是藏在json datasource 的哪裡
		$$	例如 _version 裡面存

			{
				"name": "Query",
				"type": "struct",
				"tag": "json:\"query\"",
				"Query":
				[
					{
						"name": "Stm",
						"type": "string",
						"tag": "json:\"stm\""
					}
				]
			}

		$$	代表 version的資訊存在Query裡的stm裡 (這裡的例子 stm 是 1557997753559)
	*/

	// loadconfig.DataStructure 是一個map[string]interface{} // ?? Leo: interface{}可以直接轉成[]interface{}
	// $$ 用 struct 裡每一個version的結構生成 動態的struct
	//	fmt.Println("test Leo 1")
	for ver := range loadconfig.DataStructure {
		bo_fields[ver] = make(map[string][]BO_FIELD)
		// $$ 雖然看起來只回傳 store_struct, 但其實同時也在建構 version和 field_key
		s.store_struct[ver] = datasource.initial_struct(loadconfig.DataStructure[ver].([]interface{}), ver, []string{})
	}

	return s

}

func (datasource *DATASOURCE) initial_struct(Inputs []interface{}, ver string, keys []string) reflect.Type {

	var s *All_Version_Struct = datasource.all_version_struct
	// $$ example
	// $$ 	Inputs: map[Query:[map[name:Stm tag:json:"stm" type:string]] name:Query tag:json:"query" type:struct]
	// $$ 	ver: _version

	/*
		?? Leo: keys 這個陣列放一個空的進 initial_struct(), 之後就一直被帶下去, 看起來一直是空白的(但ikey卻有值), 之後幹嘛用?
		$$ Answer: 雖然一開始從GetJsonStruct()進去時keys是空白的
		$$		, 但如果進到level x (x>1) 的結構, initial_struct() 會從 get_itemtype的遞迴進入
		$$ 		, 這時候keys就有值了, 為 x-1 的欄位名稱

		ex:
								經過 get_item 以後
			Level 1: keys: 空白 ----------------------> ikey: [Query]

				從遞迴進入Level 2

				進入for迴圈

			Level 2: keys: Query ---------------------> ikey: [Query stm]
			Level 2: keys: Query ---------------------> ikey: [Query dtm]
			......
	*/

	var field []reflect.StructField
	//var key []string

	//var key []string
	// $$ 讀該version底下每一個欄位的name, type, tag, key等屬性 (到這層絕對是陣列 Inputs []interface{})
	for i := range Inputs {
		// $$=============url 獨有==============
		if datasource.source_type == "url" {
			if Inputs[i].(map[string]interface{})["name"] == nil {
				fmt.Println("have no name")
				continue
			}
		}
		// $$=============url 獨有==============

		iname, itype, itag, ikey := datasource.get_item(Inputs[i].(map[string]interface{}), ver, keys)
		//Debug(iname," ",itype," ",itag)

		// $$ Leo: struct 的tag是幹嘛的?
		// Answer: tag 可以拿來存 `json:ooxx` 給Unmarshal用
		field = append(field, reflect.StructField{
			Name: iname, Type: itype, Tag: itag,
		})

		//=========================================
		// ?? Leo: 這段的用意?? _version 和 Source_Json裡面version的用意?
		// Answer: 如之前註解, _version是用來記錄讀進來的資料的version是藏在json datasource 的哪裡
		if ver == "_version" {
			s.version = append([]string{iname}, s.version...)
		}

		/* ?? Leo: 寫進field_key, 這是要幹嘛的?

		我以為是紀錄哪些欄位要寫進BO, 但看來沒有_bo也照寫

		1. 有 _bo
		2. 無 _bo  && (不是 struct ) && allfield_generation == true

		也就是說 如果type是string int64...那些, 又沒有_bo, 就略過

		*/

		if Inputs[i].(map[string]interface{})["_bo"] != nil {
			//			s.field_key[iname] = ikey
			prepare_bostruct(Inputs[i].(map[string]interface{})["_bo"].([]interface{}), ver, ikey)

			//Debug(key)
		} else if Inputs[i].(map[string]interface{})["type"] != "struct" && Inputs[i].(map[string]interface{})["type"] != "[]struct" && *allfield_generation {
			//			s.field_key[iname] = ikey
			prepare_bostruct(nil, ver, ikey)
		}

	}

	/*
		//把沒有紀錄過的紀錄下來
		https://play.golang.org/p/UN6dvHFs6z9
		field = append(field,reflect.StructField{
					Name: "_others", Type: map[string]interface{}, Tag: "json:\"-\"",
				})
	*/
	//Debug(field)

	/* $$
	keys:  [TableSetting X] , field:  [{Field  string  0 [] false}]
	keys:  [TableSetting] , field:  [{MainTable  string  0 [] false} {CreateTime  string  0 [] false} {X  struct { Field string }  0 [] false}]
	keys:  [] , field:  [{AppName  string json:"app_name" 0 [] false} {TableSetting  struct { MainTable string; CreateTime string; X struct { Field string } } json:"table_settings" 0 [] false}]
	*/
	//	fmt.Println("keys: ", keys, ", field: ", field)

	// ?? Leo: StructOf 怎麼用??

	return reflect.StructOf(field)

}

func (datasource *DATASOURCE) get_item(item map[string]interface{}, ver string, keys []string) (iname string, itype reflect.Type, itag reflect.StructTag, ikey []string) {
	source_type := datasource.source_type
	// $$ example:
	// $$ 	item: [map[name:Stm tag:json:"stm" type:string]]
	//	iname = item["name"].(string)

	//	var s *All_Version_Struct = &datasource.all_version_struct
	reg, err := regexp.Compile("[^a-zA-Z0-9]+") //only keep letters and number
	if err != nil {
		fmt.Println(err)
	}
	iname = reg.ReplaceAllString(strings.Title(item["name"].(string)), "") //change first letter to capital
	ikey = append(keys, iname)
	itype = datasource.get_itemtype(item, ver, ikey)

	if item["tag"] != nil {
		itag = reflect.StructTag(item["tag"].(string))
	} else {
		itag = reflect.StructTag(source_type + ":\"" + item["name"].(string) + "\"")
	}

	return iname, itype, itag, ikey
}

func (datasource *DATASOURCE) get_itemtype(item map[string]interface{}, ver string, keys []string) reflect.Type {

	key := item["name"].(string)
	var intertyp interface{}
	var intertypary []interface{}

	if item["type"] == nil {
		return reflect.ValueOf(&intertyp).Type().Elem()
	}

	switch item["type"].(string) {
	case "int":
		return reflect.TypeOf(1)
	case "[]int":
		return reflect.TypeOf([]int{1})
	case "float":
		return reflect.TypeOf(1.0)
	case "[]float":
		return reflect.TypeOf([]float64{1.0})
	case "string":
		return reflect.TypeOf("string")
	case "[]string":
		return reflect.TypeOf([]string{"string"})
	case "bool":
		return reflect.TypeOf(true)
	case "[]bool":
		return reflect.TypeOf([]bool{true})
	case "struct":
		return datasource.initial_struct(item[key].([]interface{}), ver, keys)
	case "[]struct":
		return reflect.SliceOf(datasource.initial_struct(item[key].([]interface{}), ver, keys))
	case "interface":
		return reflect.ValueOf(&intertyp).Type().Elem()
	case "[]interface":
		return reflect.ValueOf(&intertypary).Type().Elem()
	default:
		return reflect.ValueOf(&intertyp).Type().Elem()
	}

	return reflect.TypeOf("string")
}

func prepare_bostruct(bo_struct []interface{}, ver string, keys []string) {

	// $$ Leo 此function 是為了建構 boCol 和 bo_fields
	/*
	   boCol是為了存 table_setting 裡的順序
	   boColOrder 是為了要mapping "在tablesetting裡的順序" 和 "有寫 _bo 的欄位的順序"
	*/

	// $$ keys 為上一層的欄位名稱, ver是version名

	if bo_struct == nil {

		// $$ 只有當 無 _bo  && (不是 struct ) && allfield_generation == true 時才可能進來這裡
		// $$ 如果沒有 _bo 的話, 幫它長一個default的

		// $$ Leo: 把ver裡所有 "." 取代成 "_", 因為mysql的table名稱裡不能含有 "."

		tbl := fmt.Sprintf("%s_%s", topic, strings.Replace(ver, ".", "_", -1))
		wstbl := fmt.Sprintf("default.%s", tbl)
		bo_fields[ver][wstbl] = append(bo_fields[ver][wstbl], BO_FIELD{
			Workspace: "default",
			Table:     tbl,
			Fieldname: keys[len(keys)-1], // $$ 把目前的欄位名放進去, ex: [Query Lang] 則放入 Lang
			Datatype:  "varstring",
			Order:     -1, // ?? Leo: 為什麼是 -1
			Keys:      keys,
		})
	}

	bo_fields_that_not_exist_in_table_setting := 0
	// $$ 把 bo_struct 的內容照順序讀出來, 並照同樣順序塞進 bo_fields 裡 (boColOrder裡最後存的也是這個順序)
	for _, content := range bo_struct {

		//default values
		ws := "default"
		tbl := topic
		fldname := keys[len(keys)-1] // $$ 把目前的欄位名放進去, ex: [Query Lang] 則放入 Lang
		typ := "varstring"
		parser := make(map[string]interface{})

		//use user assigned value
		if content.(map[string]interface{})["workspace"] != nil {
			ws = content.(map[string]interface{})["workspace"].(string)
		}
		if content.(map[string]interface{})["table"] != nil {
			tbl = content.(map[string]interface{})["table"].(string)
		}
		if content.(map[string]interface{})["fieldname"] != nil {
			fldname = content.(map[string]interface{})["fieldname"].(string)
		}
		if content.(map[string]interface{})["datatype"] != nil {
			typ = content.(map[string]interface{})["datatype"].(string)
		}
		if content.(map[string]interface{})["_parse"] != nil {
			parser = content.(map[string]interface{})["_parse"].(map[string]interface{})
		}

		wstbl := fmt.Sprintf("%s.%s", ws, tbl)

		// $$ Jenny: TableSetting 是假設table已經存在了, 存進table時要照TableSetting的順序存
		Debug("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", loadconfig.TableSetting)

		//assign predefined column Order

		// $$ 根據 TableSetting 的內容來把欄位順序記在 boCol 裡
		// $$ wstbl: "Auto.Storm"

		if loadconfig.TableSetting[wstbl] != nil && boCol[wstbl] == nil {
			Debug(loadconfig.TableSetting[wstbl])
			boCol[wstbl] = make(map[string]int)
			for k, schema := range loadconfig.TableSetting[wstbl].([]interface{}) {
				//	fmt.Println(schema.(string), ", ", k)
				boCol[wstbl][schema.(string)] = k
				Debug("k", k)
				Debug("schema", schema)

			}

		}

		Debug(boCol[wstbl])

		// $$ table_setting的內容必須要跟真正的table schema一樣

		// $$ 不論是 boCol或bo_fields的Order, 如果 有寫 _bo 也有寫 table_setting, 則 Order 為正
		// $$ 		如果只有寫_bo, 沒寫table_setting, 則代表此欄位並不直接寫進BO, 但可能為其他欄位的input, 因此不能捨棄
		// $$			仍然要建立在boCol和bo_fields裡面, 但為了和其他正常(有寫 _bo 也有寫 table_setting)區隔, 所以把 Order 設為負

		/*
			$$	 寫 _bo	| 寫 table_setting
				============================================
				 	Y	|	Y				|	此欄位會直接寫進BO, Order為正
					N	|	N				|	多於欄位, table_setting沒定義, 也不寫進BO
					Y	|	N				|	不直接寫進BO, 但可能為其他欄位的input, Order 為負
					N	|	Y				|	塞空白

		*/

		var temp_boCol int
		var exist bool
		if temp_boCol, exist = boCol[wstbl][fldname]; !exist {
			bo_fields_that_not_exist_in_table_setting--

			boCol[wstbl][fldname] = bo_fields_that_not_exist_in_table_setting

			temp_boCol = bo_fields_that_not_exist_in_table_setting
		}

		bo_fields[ver][wstbl] = append(bo_fields[ver][wstbl], BO_FIELD{
			Workspace: ws,
			Table:     tbl,
			Fieldname: fldname,
			Datatype:  typ,
			Order:     temp_boCol,
			Keys:      keys,
			Parse:     parser,
		})
		// ======================================

		//	} else {
		//		bo_fields_that_not_exist_in_table_setting --
		//
		//			boCol[wstbl][schema.(string)] = bo_fields_that_not_exist_in_table_setting
		//
		//			bo_fields[ver][wstbl] = append(bo_fields[ver][wstbl], BO_FIELD{
		//				Workspace: ws,
		//				Table:     tbl,
		//				Fieldname: fldname,
		//				Datatype:  typ,
		//				Order:     bo_fields_that_not_exist_in_table_setting,
		//				Keys:      keys,
		//				Parse:     parser,
		//			})
		//		}

	}
}

func prepare_table() {

	// $$ Leo 此function 是為了建構 boColOrder, 還有向BO下create table的指令
	/*
		$$ bo_fields 的內容是由 prepare_bostruct() 建立起來的

		$$ wstbl: "Auto.Storm"
		$$ bo_fields[ver]["Auto.Storm"]
			= [
				{"workspace": "Auto", "table": "Storm", "fieldname":"ts", "order": 1.....}
				, {"workspace": "Auto", "table": "Storm", "fieldname":"dt", "order": 0.....}
				, {"workspace": "Auto", "table": "Storm", "fieldname":"Header", "order": 2.....}
				, {"workspace": "Auto", "table": "Storm", "fieldname":"UserAgent", "order": 3.....}
				.............
			]
	*/
	for ver, ver_content := range bo_fields {

		boColOrder[ver] = make(map[string]map[int]int)
		for _, bofield_content := range ver_content {

			/*
			   $$ 向BO要求建立table

			   queryws := NewBoQuery(bo, "bigobject")
			   sqlws := fmt.Sprintf("create Workspace %s", bofield_content[0].Workspace)
			   if _, err := queryws.Query(sqlws, 0, 1); err != nil {
			       fmt.Printf("\"%s\" fail: %v\n", sqlws, err)
			   }
			*/
			// $$ wstbl: "Auto.Storm"
			wstbl := fmt.Sprintf("%s.%s", bofield_content[0].Workspace, bofield_content[0].Table)

			boColOrder[ver][wstbl] = make(map[int]int)

			//reorder column from predefined table_setting schema
			for i, fld := range bofield_content {
				// $$ fld.Order 是從 boCol來的, 是記 json config裡 table_settings 裡的順序
				/*
				   $$
				       boColOrder[ver][Workspace.Table][在tablesetting裡的順序] =  該table裡 "_bo" 欄位的順序
				       =======================================================================
				       boColOrder[ver]["Auto.Storm"][1] = 0 // ts
				       boColOrder[ver]["Auto.Storm"][0] = 1 // dt
				       boColOrder[ver]["Auto.Storm"][2] = 2 // Host
				       boColOrder[ver]["Auto.Storm"][3] = 3 // UserAgent

				*/
				//	fmt.Println("wstbl: ", wstbl, ", fld.Order:", fld.Order, ", i: ", i)
				boColOrder[ver][wstbl][fld.Order] = i
				//	fmt.Println("in prepare_table boColOrder: ", boColOrder)
			}

			//check if predefined Table schema and assigned schema is different
			/* ?? Leo:
			       $$ boColOrder 是根據 loadconfig.datastructure 裡的每個version的 _bo 結構訂的
			           loadconfig.datastructure 裡的每個version
			           --> Inputs[i].(map[string]interface{})["_bo"].([]interface{})
			           --> bo_struct [i]interface{}
			           --> bo_fields[ver][wstbl][i]
			           --> bofield_content[i] ( 看起來好像 bofield_content[i].Order 又是從boCol[wstbl][fldname]來)

			   $$ boCol 是根據 loadconfig.TableSetting
			   // $$ ex: boCol["Auto.Storm"]["dt"] = 0, boCol["Auto.Storm"]["ts"] = 1, boCol["Auto.Storm"]["Host"] = 2 .....

			   $$ 兩個互相比對看是否欄位數量一樣
			*/
			if len(boColOrder[ver][wstbl]) != len(boCol[wstbl]) {
				//	insert_log(localtime, "data structure does not match predefined Table schema", wstbl)
				//	Debug(wstbl, "'s data structure does not match predefined Table schema")
				temp_msg := wstbl + "'s data structure does not match predefined table schema, " + strconv.Itoa(len(boColOrder[ver][wstbl])) + ":" + strconv.Itoa(len(boCol[wstbl]))
				fmt.Println(temp_msg)
				//		log.Fatal(temp_msg)
			}

			//create table schema in predefined or default Order
			var sqlqry string
			for i := 0; i < len(boColOrder[ver][wstbl]); i++ {

				fldidx := boColOrder[ver][wstbl][i]
				Debug(i, "------ ", fldidx)
				if sqlqry == "" {
					sqlqry = fmt.Sprintf("CREATE DEFAULT COLUMN TABLE %s (", wstbl)
				}
				// $$ 按照 boColOrder 紀錄的順序從 bofield_content裡提出來, 將table 的欄位照此順序create出來
				/*
				   $$
				       if i = 0
				       , then bofield_content[boColOrder[ver]["Auto.Storm"][i]].Fieldname
				       , then bofield_content[1].Fieldname
				       , then "dt"
				*/
				sqlqry = fmt.Sprintf("%s '%s' %s,", sqlqry, bofield_content[fldidx].Fieldname, bofield_content[fldidx].Datatype)
			}
			sqlqry = fmt.Sprintf("%s 'autogenerated_source' string, 'autogenerated_dt' datetime32, 'autogenerated_ts' timestamp)", sqlqry)

			/*
			   $$ 向BO要求建立table
			   if _, err := queryws.Query(sqlqry, 0, 1); err != nil {
			       fmt.Printf("\"%s\" fail: %v\n", sqlqry, err)
			   }
			*/
			Debug(sqlqry)

		}
	}
	//	fmt.Println("in prepare_table, boColOrder", boColOrder)
}

// customfunc, colData[ver], target_table, target_key, bofield_content, boColOrder[ver], boCol, Parse_result_cache                           map[string][]map[string]interface{}
func parse_func(customfunc []*plugin.Plugin, records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (string, map[string]map[string]string, bool, error) {

	var output interface{}
	var row_success bool = true
	var parse_func_err error

	//map[ver][Workspace.Table][ boColOrder[ver][Workspace.Table][ boCol[Workspace.Table][key] ] ]
	var exist_in_cache bool
	if output, exist_in_cache = Parse_result_cache[target_table][target_key]; !exist_in_cache { // $$ 在Cache裡面不存在

		order_in_table_setting := boCol[target_table][target_key]
		order_in_bo_struct := boColOrder_ver[target_table][order_in_table_setting]

		// map[string][]interface{}
		FuncName := bo_fields_map_ver[target_table][order_in_bo_struct]["Parse"].(map[string]interface{})["func"].(string)

		//	FuncName := output_parse["func"].(string)
		//	fmt.Println("FuncName:", FuncName)

		if _, ok := CacheFunc[FuncName]; ok {
			output, Parse_result_cache, row_success, parse_func_err = CacheFunc[FuncName].(func(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (output interface{}, Parse_Result_Cache map[string]map[string]string, row_success bool, err error))(records, target_table, target_key, bo_fields_map_ver, boColOrder_ver, boCol, Parse_result_cache)
		} else {
			func_not_found := true
			for _, checkfunc := range customfunc { // $$ 掃過所有的.so檔

				getcustomfunc, err := checkfunc.Lookup(FuncName)
				if err == nil {
					func_not_found = false
					//	CacheFunc[FuncName] = getcustomfunc
					output, Parse_result_cache, row_success, parse_func_err = getcustomfunc.(func(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (output interface{}, Parse_Result_Cache map[string]map[string]string, row_success bool, err error))(records, target_table, target_key, bo_fields_map_ver, boColOrder_ver, boCol, Parse_result_cache)
					break
				}
			}
			if func_not_found { // $$ 掃過所有.so檔都找不到 FuncName
				temp_error_string := FuncName + " not found"
				parse_func_err = fmt.Errorf(temp_error_string)
				row_success = false // $$ 整筆資料都丟掉
				output = ""
			}

		}
	}
	return InterfaceToStr(output), Parse_result_cache, row_success, parse_func_err
}

func List_redundant_fields(input_keys map[string]interface{}, Value_SourceData reflect.Value) map[string]interface{} {
	//	fmt.Println("=====================================================")
	//	fmt.Println("input_keys: ", input_keys)
	//	fmt.Println("Value_SourceData.Type().String(): ", Value_SourceData.Type().String())
	for i := 0; i < Value_SourceData.NumField(); i++ { // $$ 搜尋config裡的key
		Value_EachField := Value_SourceData.Field(i)
		StructField_EachField := Value_SourceData.Type().Field(i)

		key, tag_found := StructField_EachField.Tag.Lookup("json")
		//	fmt.Println("key: ", key, ", type: ", Value_EachField.Type().Kind().String())
		if input_keys[key] != nil && tag_found { // $$ 在input裡面找到 config裡的key
			switch Value_EachField.Type().Kind().String() {

			case "struct":
				next_level_keys := make(map[string]interface{})
				//========================

				type_of_input_keys := reflect.TypeOf(input_keys[key])
				//		fmt.Println("type_of_input_keys:", type_of_input_keys)
				if type_of_input_keys == reflect.TypeOf([]string{"sss"}) {
					// $$ 把json格式的input_keys[key] 轉成 map
					if Unmarshal_err := json.Unmarshal([]byte(InterfaceToStr(input_keys[key])), &next_level_keys); Unmarshal_err != nil {
						fmt.Println(Unmarshal_err)
						break
					}
				} else {
					var temp_map map[string]interface{}
					if type_of_input_keys == reflect.TypeOf(temp_map) {
						next_level_keys = input_keys[key].(map[string]interface{})
					}
				}
				input_keys[key] = List_redundant_fields(next_level_keys, Value_EachField)
				//	fmt.Println("input_keys[", key, "]:", input_keys[key])
				if len(input_keys[key].(map[string]interface{})) <= 0 {
					delete(input_keys, key)
					//		fmt.Println("After delete ", key, ": ", input_keys)
				}

			default:
				delete(input_keys, key)
				//	fmt.Println("After delete ", key, ": ", input_keys)
			}
		}
	}

	return input_keys
}

//====================streamer 3.0 test above===============================

func put_outer_data_in_json(source_data []byte) ([]byte, error) {
	var output string
	var err error
	source_data_str := string(source_data)

	//============================
	// $$ 首先我想先確認source_data至少有個json, 用 {(.+)} 至少可以拿到最外面那層json
	// $$ 這個json內容合不合法, 我先不管, 交給外層的function判斷
	// $$ 不能用 (.*){(.+)}, 如果json有很多層就會拿到裡面那層json, 會錯
	regexp_json := regexp.MustCompile("{(.+)}").FindStringSubmatch(source_data_str)
	//	fmt.Println(regexp_json)
	//	fmt.Println(len(regexp_json))
	if len(regexp_json) == 2 {
		output = regexp_json[1]

		// $$ 嘗試取得 time + ip
		time_sys := ""
		first_left_bracket := strings.Index(source_data_str, "[")
		first_right_bracket := strings.Index(source_data_str, "]")
		//	fmt.Println(first_left_bracket, ", ", first_right_bracket)
		if first_left_bracket >= 0 && first_right_bracket >= 0 {
			time_sys = strings.Trim(source_data_str[first_left_bracket+1:first_right_bracket], " ")
			//	fmt.Println("time_sys:", time_sys)
		}
		if time_sys != "" {
			output = output + "," + "\"time_sys\"" + ":" + "\"" + time_sys + "\""
		}
		ip_gama := ""
		first_comma := strings.Index(source_data_str, ",")
		if first_comma >= 0 {
			first_bracket := strings.Index(source_data_str, "{")
			if first_comma < first_bracket {
				// $$ , 在json前面
				substring := source_data_str[first_comma+1 : first_bracket]

				first_left_bracket = strings.Index(substring, "[")
				first_right_bracket = strings.Index(substring, "]")
				if first_left_bracket >= 0 && first_right_bracket >= 0 {
					ip_gama = strings.Trim(substring[first_left_bracket+1:first_right_bracket], " ")
					//		fmt.Println("ip_gama:", ip_gama)
				}
			}
		}
		if ip_gama != "" {
			output = output + "," + "\"ip_gama\"" + ":" + "\"" + ip_gama + "\""
		}

	} else {
		err = fmt.Errorf("source data does not contain json")
		//		fmt.Println(err)
	}

	output = "{" + output + "}"

	return []byte(output), err
}

func Append_error(first_error error, second_error error) error {
	var divider string = ""
	if first_error != nil {
		divider = "; "
	}
	return fmt.Errorf(first_error.Error() + divider + second_error.Error())
}

func replace_invalid_characters(source_data []byte) ([]byte, error) {
	//	var output string
	var err error
	source_data_str := string(source_data)

	source_data_str = strings.ReplaceAll(source_data_str, "\"{\\\"", "{\"")
	source_data_str = strings.ReplaceAll(source_data_str, "\\\"}\"", "\"}")
	source_data_str = strings.ReplaceAll(source_data_str, "\\\":\\\"", "\":\"")
	source_data_str = strings.ReplaceAll(source_data_str, "\\\",\\\"", "\",\"")
	source_data_str = strings.ReplaceAll(source_data_str, "\\\\n", "")
	source_data_str = strings.ReplaceAll(source_data_str, "\\n", "")
	source_data_str = strings.ReplaceAll(source_data_str, "\\r", "")

	return []byte(source_data_str), err
}

func turn_bo_struct_to_map(bo_fields map[string]map[string][]BO_FIELD, bo_fields_map map[string]map[string][]map[string]interface{}) (map[string]map[string][]map[string]interface{}, error) {
	// bo_fields_map =  map[string]map[string][]map[string]interface{}
	var err_msg error
	if temp_json, err := json.Marshal(bo_fields); err != nil {
		err_msg = err
	} else {
		err_msg = json.Unmarshal([]byte(temp_json), &bo_fields_map)
	}
	//	fmt.Println(string(temp_json))

	return bo_fields_map, err_msg
}
