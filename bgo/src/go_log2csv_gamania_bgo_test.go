// package name: json2csv
package main

import (
    "bytes"
    "encoding/csv"
    "log"
    //"regexp"
    //"strings"
    "syscall"
    "testing"
    "time"

    //"github.com/oschwald/geoip2-golang"
    //"github.com/ua-parser/uap-go/uaparser"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)


type CDNLogTestSuite struct {
	suite.Suite
}

func (suite *CDNLogTestSuite) SetupTest() {
    var err error

    if BOTz, err = time.LoadLocation("Asia/Taipei"); err != nil {
        log.Fatalln("Load TW time zone fail:", err)
    }

    //if GeoDB, err = geoip2.Open("../common/" + GEO_DB_NAME); err != nil {
    //    log.Fatalln("Open geo db fail:", err)
    //}

    //if UAParser, err = uaparser.New("../common/" + UA_PARSER_YAML); err != nil {
    //    log.Fatalln("New ua parser fail:", err)
    //}

    // Compiled regexp
    //if len(CompiledReferRegexp) != len(ReReferSources) {
    //    log.Fatalln("Referer regexp structure mismatch")
    //}
    //for i, re := range ReReferSources {
    //    if CompiledReferRegexp[i], err = regexp.Compile(re.Clause); err != nil {
    //        log.Fatalf("Compile \"%s\" fail: %v\n", re.Clause, err)
    //    }
    //}
//
    //if len(CompiledUtmRegexp) != len(ReUtmSources) {
    //    log.Fatalln("Utm regexp structure mismatch")
    //}
    //for i, re := range ReUtmSources {
    //    if CompiledUtmRegexp[i], err = regexp.Compile(re.Clause); err != nil {
    //        log.Fatalf("Compile \"%s\" fail: %v\n", re.Clause, err)
    //    }
    //}


    OutDir = "/tmp"
}

func (suite *CDNLogTestSuite) Testlog() {
    var data = `[2019-07-31 05:57:04],[49.217.205.24]{"uid":"449d5af8595e4dbaace2cf9903fe3d46_usr","osType":"android","androidIdfa":"d8e36140-5ab8-4c93-b56b-5368557af914","deviceModel":"SM-J810Y","osVersion":"9","carrier":"Far EasTone","networkType":"mobile","version":"1.9.33","tz":"Asia\/Taipei","latitude":"null","longitude":"null","dtm":"1564587041992","action_detail":{"verify_key":"cc46ba23e3f8eb67e928594341f715f5","open_id":"1014315000811102516","agent_name":"app","tracking_version":"0.02","event_id":"401","action":"chatroom_click_interactive_msg","field":{"chatroom_type":"0","option_url":"https:\/\/newh5.gashpoint.com\/Mission\/CardDetailByPush?SID=15938","option_txt":"快點我!馬上領取任務吧!","option_action":"h5_page","chatroom_title":"GASH 小幫手","chatroom_id":"dbe3db8e80424d028db80525dddf6a37_oarm","msg_received_time":"1554512533518","msg_txt":"\"早上09:00了，該起床了吧!!\r\n開個bf!領點數當早鳥族吧!限時一小時請盡速領取囉!\"\r\n","oaid":"007bb2bc77e241febf5f2c30890bc9bb_oa"}},"co":{"schema":"iglu:com.snowplowanalytics.snowplow\/client_session\/jsonschema\/1-0-1","data":{"sessionIndex":0,"storageMechanism":"SQLITE","firstEventId":"401","sessionId":null,"previousSessionId":null,"userId":null}},"stm":"1564587041993"}`

    //rSrc := csv.NewReader(strings.NewReader(data))
    //rSrc.Comma = '\t'
    //cdnlog, _ := rSrc.ReadAll()
    var s []string
    cdnlog := append(s,data)

    fdCdn := TranslateCdn(cdnlog)

    assert.NotEqual(suite.T(), fdCdn, -1)

    buf := make([]byte, 4096)
    syscall.Seek(fdCdn, 0, 0)
    nRead, err := syscall.Read(fdCdn, buf)
    assert.NotEqual(suite.T(), nRead, 0)
    assert.NoError(suite.T(), err)
    
    rTgt := csv.NewReader(bytes.NewReader(buf[:nRead]))
    rows, err := rTgt.ReadAll()
    assert.Equal(suite.T(), len(rows), 1)
    assert.Equal(suite.T(), len(rows[0]), 52)

    expected := []string{"Far EasTone", "SM-J810Y", "null", "null", "mobile", "android", "9", "2019-07-31 23:30:41.993 +0800 CST", "Asia/Taipei", "449d5af8595e4dbaace2cf9903fe3d46_usr", "1.9.33", "d8e36140-5ab8-4c93-b56b-5368557af914", "chatroom_click_interactive_msg", "app", "cc46ba23e3f8eb67e928594341f715f5", "0.02", "", "49.217.205.24", "1014315000811102516", "401", "", "", "007bb2bc77e241febf5f2c30890bc9bb_oa", "", "", "", "", "GASH 小幫手", "0", "dbe3db8e80424d028db80525dddf6a37_oarm", "", "2019-04-06 09:02:13.518 +0800 CST", "\"早上09:00了，該起床了吧!!\n開個bf!領點數當早鳥族吧!限時一小時請盡速領取囉!\"\n", "", "", "", "", "", "", "h5_page", "https://newh5.gashpoint.com/Mission/CardDetailByPush?SID=15938", "快點我!馬上領取任務吧!", "", "401", "", "0", "SQLITE", "2019-08-01 15:10:25", "1564523824", "-999", "-999", "2019-07-31 23:30:41.992 +0800 CST", "1564587041993"}
    assert.Equal(suite.T(), expected, rows[0])
}


// TestCDNLogTestSuite invokes a normal testcase for cdnlog testsuite.
func TestCDNLogTestSuite(t *testing.T) {
	suite.Run(t, new(CDNLogTestSuite))
}
