package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"reflect"
	"strconv"
)

// $$ reference https://play.golang.org/p/kwc32A5mJf
// ?? 把version_struct一整個structure傳進來, 用reader讀一行然後寫進version_struct裡面
func UrlUnmarshal(url_map url.Values, Value_SourceData reflect.Value) (success bool, marshal_err error) {
	//	record, err := reader.Read()
	//	if err != nil {
	//		fmt.Printf("csv unmarshall read fail: %v\n", err)
	//	}

	//====================================
	success = true
	var temp_err_string string
	fmt.Println("UrlUnmarshal: ")
	fmt.Println(url_map)

	if Value_SourceData.NumField() != len(url_map) {
		temp_err_string = "data structure number does not match url table column number " + strconv.Itoa(Value_SourceData.NumField()) + " != " + strconv.Itoa(len(url_map))
		marshal_err = errors.New(temp_err_string)
		success = false
	}

	//======================================

	// $$ v是Value的指標(*Value), 可視為Value的陣列, 所以可透過 *Value.Field(int) 來取得個別Value
	// $$ *Value.Type().Field(i) 得到的是StructField
	// $$ *Value.Field(i) 得到的卻是 Value
	for i := 0; i < Value_SourceData.NumField(); i++ {
		Value_EachField := Value_SourceData.Field(i)
		StructField_EachField := Value_SourceData.Type().Field(i) // $$ 相當於 Type_SourceData.Field(i)

		//	key := StructField_EachField.Name
		key, tag_found := StructField_EachField.Tag.Lookup("json")

		fmt.Println("key: ", key)

		if url_map[key] != nil && tag_found {
			//	val := Value_SourceData.Field(i) // $$ 用*Value.Field(int) 來取得個別Value
			switch Value_EachField.Type().Kind().String() { // $$ 如果沒有 Kind(), 會拿不到struct的Type
			case "string":
				Value_EachField.SetString(url_map.Get(key)) // $$ url_map[key][0] 也可以
			case "int":
				if int_val, err := strconv.ParseInt(url_map.Get(key), 10, 0); err == nil {
					Value_EachField.SetInt(int_val)
				} else {
					temp_err_string = "url unmarshall parse int fail: " + err.Error()
					marshal_err = errors.New(temp_err_string)
					//	success = false
				}

			case "struct":
				fmt.Println("in struct: ", key, ": ", url_map.Get(key))
				newstruct := reflect.New(StructField_EachField.Type).Interface() // $$ StructField_EachField.Type == Value_EachField.Type() 也可以
				if err := json.Unmarshal([]byte(url_map.Get(key)), &newstruct); err == nil {
					Value_EachField.Set(reflect.ValueOf(newstruct).Elem())
				} else {
					temp_err_string = "url unmarshall parse struct fail: " + err.Error()
					marshal_err = errors.New(temp_err_string)
				}
				fmt.Println("newstruct: ", newstruct)
			default:
				Value_EachField.Set(reflect.ValueOf(url_map.Get(key)))
			}
		}

	}
	//=======================================
	/*
		for i := 0; i < v.NumField(); i++ {
			fmt.Println("record[", i, "]: ", record[i])
			f := v.Field(i)
			switch f.Type().String() {
			case "string":
				f.SetString(record[i])
			case "int":
				ival, err := strconv.ParseInt(record[i], 10, 0)
				if err != nil {
					temp_err_string = "url unmarshall parse int fail: " + err.Error()
					marshal_err = errors.New(temp_err_string)
					success = false
				}
				f.SetInt(ival)
			default:
				f.Set(reflect.ValueOf(record[i]))
			}
		}
	*/
	return success, marshal_err
}
